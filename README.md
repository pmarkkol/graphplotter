# Graphplotter #

Web application of [Calculator](https://bitbucket.org/pmarkkol/calculator) project. This web application is built using Vaadin framework. 

### Requirements ###

 - Java Platform, Standard Edition 8. See http://www.oracle.com/technetwork/java/javase/overview/index.html for install instructions.
 - Maven. See https://maven.apache.org/ for more info.
 - git. See https://git-scm.com/ for more info.
 - Web server that implements Java EE specified Java servlets, such as Tomcat, GlassFish or Wildfly.


### Instructions ####

Make sure that Java, maven and git are properly installed. To verify run commands

	javac -version 
	mvn --version
	git --version

In case of error check the install instructions for these software. 

Checkout Graphplotter source from repository with command:

	git clone https://pmarkkol@bitbucket.org/pmarkkol/graphplotter.git


Move to the directory containing the source with

	cd graphplotter
	
Compile source and create web archive (war) package using maven 
	
	mvn package

To test the application use the war package from graphplotter-ui/target directory to deploy the application to your web server.


### Contact ###

Paavo Markkola (paavo.markkola@iki.fi)