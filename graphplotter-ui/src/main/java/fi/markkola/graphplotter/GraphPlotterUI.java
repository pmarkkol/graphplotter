package fi.markkola.graphplotter;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.annotations.Widgetset;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.event.FieldEvents.BlurEvent;
import com.vaadin.event.FieldEvents.BlurListener;
import com.vaadin.event.FieldEvents.FocusEvent;
import com.vaadin.event.FieldEvents.FocusListener;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.shared.Position;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import fi.markkola.calculator.expression.EvaluationErrorException;
import fi.markkola.calculator.expression.InvalidExpressionException;
import fi.markkola.calculator.expression.PostfixSolver;
import fi.markkola.calculator.graph.Graph;
import fi.markkola.calculator.graph.GraphLimits;
import fi.markkola.calculator.graph.GraphPoint;
import fi.markkola.graphplotter.graphpanel.GraphPanel;

/**
 * Vaadin framework based user interface for graph plotter application.
 *
 * @author Paavo Markkola
 */
@SuppressWarnings("serial")
@Theme("GraphPlotterTheme")
@Widgetset("fi.markkola.graphplotter.GraphPlotterWidgetset")
public class GraphPlotterUI extends UI {
	/**
	 * Layouts that contains all components.
	 */
	private VerticalLayout applicationFrame;
	private HorizontalLayout expressionPanel;
	private VerticalLayout cursorPanel;
	private HorizontalLayout cursorShowPanel;
	private HorizontalLayout cursorControlPanel;
	private HorizontalLayout cursorPositionPanel;
	private VerticalLayout cursorValuesPanel;
	/**
	 * Widget used to display a graph.
	 */
	private GraphPanel graphPanel;
	/**
	 * Default size for the graph panel.
	 */
	private int graphPanelWidth = 500;
	private int graphPanelHeight = 500;
	/**
	 * Notifications to display errors for the expression.
	 */
	private Notification invalidExpresionNotification;
	private Notification evaluateErrorNotification;
	private Notification expressionInstructionsNotification;
	/**
	 * Labels for showing real value at cursor point.
	 */
	private Label lblValueForX;
	private Label lblValueForY;
	/**
	 * The graph as mathematical expression as provided by the user. Must be
	 * set and be valid for the graph to be drawn.
	 */
	private String expression;
	/**
	 * PostfixSolver instance that is used to calculate values for each point
	 * on the graph.
	 */
	private PostfixSolver postfixSolver;
	/**
	 * The class that handles building of the graph from the expression string.
	 */
	private Graph graph;
	/**
	 * Minimum and maximum limits on x and y axes for the graph.
	 */
	private GraphLimits graphLimits;
	/**
	 * When cursor is enable this point contains the location of the cursor.
	 */
	private GraphPoint cursorPoint;
	/**
	 * Index of the graph where cursor is currently located.
	 */
	private int cursorIndex;
	/**
	 * Boolean indicating whether cursor is to be displayed or not.
	 */
	private boolean cursorShow = false;
	/**
	 * Boolean indicating whether the graph needs to be recalculated or not.
	 * Recalculation is needed for example when limits are changed.
	 */
	private boolean graphUpdateNeeded;
	/**
	 * Initialize user interface.
	 */
	@Override
	protected void init(VaadinRequest vaadinRequest) {
		initExpressionPanel();
		initGraphPanel();
		initCursorInfoPanels();
		initApplicationFrame();
		initNotifications();
		initGraph();
	}
	/**
	 * Initialize expression panel. This Panel contains a text field used to
	 * enter mathematical expressions as well as buttons to plot the expression
	 * and to remove the expression from the panel.
	 */
	private void initExpressionPanel() {
		expressionPanel = new HorizontalLayout();
		expressionPanel.setSpacing(true);
		TextField textFieldExpression = new TextField("Expression:");
		textFieldExpression.setInputPrompt("y=f(x)");
		textFieldExpression.setWidth(300, Unit.PIXELS);
		Button btnHelp = new Button("?");
		Button btnDraw = new Button("Draw");
		Button btnClear = new Button("Clear");
		expressionPanel.addComponent(textFieldExpression);
		expressionPanel.addComponent(btnHelp);
		expressionPanel.addComponent(btnDraw);
		expressionPanel.addComponent(btnClear);
		expressionPanel.setComponentAlignment(textFieldExpression, Alignment.BOTTOM_LEFT);
		expressionPanel.setComponentAlignment(btnHelp, Alignment.BOTTOM_RIGHT);
		expressionPanel.setComponentAlignment(btnDraw, Alignment.BOTTOM_RIGHT);
		expressionPanel.setComponentAlignment(btnClear, Alignment.BOTTOM_RIGHT);

		btnHelp.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				handleHelpButtonEvent();
			}
		});

		btnDraw.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				handleDrawButtonEvent(textFieldExpression.getValue());
			}
		});

		btnClear.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				textFieldExpression.setValue("");
				handleDrawButtonEvent(textFieldExpression.getValue());
			}
		});

		textFieldExpression.addBlurListener(new BlurListener() {
			@Override
			public void blur(BlurEvent event) {
				btnDraw.removeClickShortcut();
			}
		});

		textFieldExpression.addFocusListener(new FocusListener() {
			@Override
			public void focus(FocusEvent event) {
				btnDraw.setClickShortcut(KeyCode.ENTER);
			}
		});
	}
	/**
	 * Initialize graph panel. Create new GraphPanel instance and set default
	 * values for it.
	 */
	private void initGraphPanel() {
		graphPanel = new GraphPanel();
		graphPanel.setSize(graphPanelWidth, graphPanelHeight);
		graphPanel.setExpression("");
		graphPanel.setLimits(-5, 5, -5, 5);
		graphPanel.setOrigo(250, 250);
		graphPanel.clearCursor();
	}
	/**
	 * Initialize cursor related panels.
	 */
	private void initCursorInfoPanels() {
		initCursorControlPanel();
		initCursorValuesPanel();
		initCursorPositionPanel();
		initCursorShowPanel();
		initCursorPanel();
		resetCursor();
		updateCursorPoint();
	}
	/**
	 * Initialize cursor control panel. This panel contains button to move
	 * cursor left and right along the graph.
	 */
	private void initCursorControlPanel() {
		cursorControlPanel = new HorizontalLayout();
		Label lblMove = new Label("Move cursor:");
		Button btnLeft = new Button("Left");
		Button btnRight = new Button("Right");
		cursorControlPanel.addComponent(lblMove);
		cursorControlPanel.addComponent(btnLeft);
		cursorControlPanel.addComponent(btnRight);
		cursorControlPanel.setComponentAlignment(lblMove, Alignment.MIDDLE_LEFT);
		cursorControlPanel.setSpacing(true);
		cursorControlPanel.setVisible(false);

		btnLeft.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				moveCursorToLeft();
			}
		});

		btnRight.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				moveCursorToRight();
			}
		});
	}
	/**
	 * Initialize cursor values panel. This panel contains labels that show
	 * current position of the cursor on the graph.
	 */
	private void initCursorValuesPanel() {
		cursorValuesPanel = new VerticalLayout();
		lblValueForX = new Label();
		lblValueForY = new Label();
		cursorValuesPanel.addComponent(lblValueForX);
		cursorValuesPanel.addComponent(lblValueForY);
	}
	/**
	 * Initialize cursor position panel.
	 */
	private void initCursorPositionPanel() {
		cursorPositionPanel = new HorizontalLayout();
		Label lblCursorAt = new Label("Cursor at:");
		cursorPositionPanel.addComponent(lblCursorAt);
		cursorPositionPanel.addComponent(cursorValuesPanel);
		cursorPositionPanel.setSpacing(true);
		cursorPositionPanel.setVisible(false);
	}
	/**
	 * Initialize cursor check box panel. This panel contains a check box that
	 * is used to show and hide cursor.
	 */
	private void initCursorShowPanel() {
		CheckBox chbxShowCursor = new CheckBox("Show cursor", cursorShow);
		chbxShowCursor.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				setShowCursor(chbxShowCursor.getValue());
				cursorControlPanel.setVisible(cursorShow);
				cursorPositionPanel.setVisible(cursorShow);
				updateCursorPoint();
			}
		});
		cursorShowPanel = new HorizontalLayout();
		cursorShowPanel.addComponent(chbxShowCursor);
		cursorShowPanel.setComponentAlignment(chbxShowCursor, Alignment.TOP_LEFT);
		cursorShowPanel.setSpacing(true);
	}
	/**
	 * Initialize main cursor panel.
	 */
	private void initCursorPanel() {
		cursorPanel = new VerticalLayout();
		cursorPanel.setWidth(graphPanelWidth, Unit.PIXELS);
		cursorPanel.addComponent(cursorShowPanel);
		cursorPanel.addComponent(cursorControlPanel);
		cursorPanel.addComponent(cursorPositionPanel);
		cursorPanel.setComponentAlignment(cursorShowPanel, Alignment.MIDDLE_LEFT);
		cursorPanel.setComponentAlignment(cursorControlPanel, Alignment.MIDDLE_LEFT);
		cursorPanel.setSpacing(true);
	}
	/**
	 * Initialize layout that contains all of the application.
	 */
	private void initApplicationFrame() {
		applicationFrame = new VerticalLayout();
		applicationFrame.setMargin(true);
		applicationFrame.setSpacing(true);
		applicationFrame.addComponent(expressionPanel);
		applicationFrame.addComponent(graphPanel);
		applicationFrame.addComponent(cursorPanel);
		applicationFrame.setComponentAlignment(expressionPanel, Alignment.MIDDLE_CENTER);
		applicationFrame.setComponentAlignment(graphPanel, Alignment.MIDDLE_CENTER);
		applicationFrame.setComponentAlignment(cursorPanel, Alignment.MIDDLE_CENTER);
		setContent(applicationFrame);
	}
	/**
	 * Initialize notifications used by the application.
	 */
	private void initNotifications() {
		expressionInstructionsNotification =
				new Notification("Instructions", "",
								 Notification.Type.ASSISTIVE_NOTIFICATION,
								 false);
		expressionInstructionsNotification.setPosition(Position.BOTTOM_CENTER);
		invalidExpresionNotification =
				new Notification("Error while parsing expression", "",
								 Notification.Type.ERROR_MESSAGE, true);
		invalidExpresionNotification.setPosition(Position.TOP_CENTER);
		evaluateErrorNotification =
				new Notification("Error while evaluating expression", "",
								 Notification.Type.ERROR_MESSAGE, false);
		evaluateErrorNotification.setPosition(Position.TOP_CENTER);
	}
	/**
	 * Put notifications for instructions on screen.
	 */
	protected void handleHelpButtonEvent() {
		expressionInstructionsNotification.
						setDescription(postfixSolver.getUserInstructions());
		expressionInstructionsNotification.setDelayMsec(-1);
		expressionInstructionsNotification.show(Page.getCurrent());
	}
	/**
	 *
	 * @param expression	Mathematical expression in string format.
	 */
	private void handleDrawButtonEvent(String expression) {
		try {
			drawGraph(expression);
		} catch (InvalidExpressionException exception) {
			invalidExpresionNotification.setDescription(exception.getMessage());
			invalidExpresionNotification.show(Page.getCurrent());
		} catch (EvaluationErrorException exception) {
			evaluateErrorNotification.setDescription(exception.getMessage());
			evaluateErrorNotification.show(Page.getCurrent());
		}
		resetCursor();
		updateCursorPoint();
	}
	/**
	 * Initialize graph with default values.
	 */
	private void initGraph() {
		setExpression(null);
		setPostfixCalculator();
		setGraphLimits();
		setGraph();
		setGraphPoints();
	}
	/**
	 * Set the string given as parameter as the expression for the graph.
	 *
	 * @param expression	Mathematical expression for the graph as a string.
	 */
	private void setExpression(String expression) {
		this.expression = expression;
	}
	/**
	 * Parse expression string, if it has been set and is not an empty string,
	 * using configured PostfixSolver instance. Create new PostfixSolver
	 * instance in case it does not yet exist.
	 */
	private void setPostfixCalculator() {
		if (postfixSolver == null)
			postfixSolver = new PostfixSolver();
		if (expression != null && !expression.isEmpty())
			postfixSolver.parseExpression(expression);
	}
	/**
	 * Set default limits for the graph. Create new instance of GraphLimits if
	 * graph limits have not yet been initialized.
	 */
	private void setGraphLimits() {
		if (graphLimits == null)
			graphLimits = new GraphLimits();
		else
			setDefaultLimits();
	}
	/**
	 * Restores coordinate limits back to default values for both x and y axes.
	 */
	private void setDefaultLimits() {
		graphLimits.setDefaultLimits();
	}
	/**
	 * Configure a new instance of Graph class. Graph is set to null
	 * if expression string has not been set or is empty.
	 */
	private void setGraph() {
		if (expression == null || expression.isEmpty())
			graph = null;
		else {
			graph = new Graph();
			graph.setExpressionSolver(postfixSolver);
			setGraphDimensions();
			setGraphUpdate(true);
		}
	}
	/**
	 * Set dimensions for the Graph object. This includes limits and number of
	 * discrete points on x and y axes.
	 */
	private void setGraphDimensions() {
		if ((graph != null) && (graphLimits != null)) {
			graph.setGraphLimits(graphLimits);
			graph.setNumberOfPointsForX(graphPanelWidth);
			graph.setNumberOfPointsForY(graphPanelHeight);
		}
	}
	/**
	 * Update the list of coordinate points representing a graph. New value
	 * for each point is calculated only if update was requested earlier.
	 */
	private void setGraphPoints() {
		if (graph != null) {
			if (graphUpdateNeeded)
				graph.setGraph();
			setGraphUpdate(false);
		}
	}
	/**
	 * Set whether graph should be updated and recalculated at next call to
	 * paintComponent.
	 *
	 * @param update	True if the graph needs an update, false otherwise
	 */
	private void setGraphUpdate(boolean update) {
		graphUpdateNeeded = update;
	}
	/**
	 * Draws a new graph according to the mathematical expression given as a
	 * parameter.
	 *
	 * @param expression	Mathematical expression for the graph as a string.
	 */
	public void drawGraph(String expression) {
		setGraphUpdate(true);
		setExpression(expression);
		setPostfixCalculator();
		setDefaultLimits();
		setGraph();
		setGraphDimensions();
		setGraphPoints();
		sendExpression();
		sendGraph();
	}
	/**
	 * Send the expression string to the GraphPanel instance.
	 */
	private void sendExpression() {
		graphPanel.setExpression(expression);
	}
	/**
	 * Update the graph to the GraphPanel instance. First, clear any existing
	 * graph that the panel might use and then go through the graph one point
	 * at a time to update graph. When done send the graph to the panel.
	 */
	private void sendGraph() {
		graphPanel.clearGraph();
		if (graph != null)
			for (GraphPoint point : graph.getGraph())
				if (point.yPixel > graphPanelHeight)
					graphPanel.addGraphPoint(point.xPixel, point.yPixel);
				else
					graphPanel.addGraphPoint(point.xPixel,
											 graphPanelHeight - point.yPixel);
		graphPanel.updateGraph();
	}
	/**
	 * Set cursor point if the graph is properly set.
	 */
	private void setCursorPoint() {
		if ((graph == null) || (graph.getGraph() == null))
			return;
		if (cursorIndex == -1)
			cursorIndex = graph.getZeroIndex();
		if (isValidCursorIndex(cursorIndex))
			cursorPoint = graph.getGraph().get(cursorIndex);
		else
			cursorPoint = null;
	}
	/**
	 * Determine if the point on a graph at given index is a valid cursor
	 * point. For the point to be a valid cursor point, the graph must have
	 * been properly set, the index must fall within the graph and the point
	 * must be visible.
	 *
	 * @param index		Index of the point on a graph to be checked.
	 */
	private boolean isValidCursorIndex(int index) {
		if ((graph == null) || (graph.getGraph() == null))
			return false;
		if ((index < 0) || (index >= graph.getGraph().size()))
			return false;
		GraphPoint point = graph.getGraph().get(index);
		if ((point.xPixel < 0) || (point.yPixel < 0) ||
			(point.xPixel > graphPanelWidth) ||
			(point.yPixel > graphPanelHeight))
			return false;
		return true;
	}
	/**
	 * Set or clear cursor point from the GraphPanel instance.
	 */
	private void setGraphPanelCursorPoint() {
		if ((cursorShow) && (cursorPoint != null))
			graphPanel.setCursor(cursorPoint.xPixel,
							Math.abs(cursorPoint.yPixel - graphPanelHeight));
		else
			graphPanel.clearCursor();
	}
	/**
	 * Show or hide cursor.
	 *
	 * @param show	True if cursor is to be shown and false if not
	 */
	private void setShowCursor(boolean show) {
		cursorShow = show;
	}
	/**
	 * Reset cursor position.
	 */
	private void resetCursor() {
		cursorIndex = -1;
		cursorPoint = null;
	}
	/**
	 * Moves cursor to left by one point along the graph. If new position for
	 * the cursor is not valid, i.e., the position does not exist, then finds
	 * the next valid position on graph. If there is no valid position on the
	 * left then cursor will remain on the old position.
	 */
	private void moveCursorToLeft() {
		if ((!cursorShow) || (cursorIndex == -1))
			return;
		for (int index = cursorIndex - 1; index >= 0; index--) {
			if (isValidCursorIndex(index)) {
				cursorIndex = index;
				cursorPoint = graph.getGraph().get(cursorIndex);
				break;
			}
		}
		updateCursorPoint();
	}
	/**
	 * Moves cursor to right by one point along the graph. If new position for
	 * the cursor is not valid, i.e., the position does not exist, then finds
	 * the next valid position on graph. If there is no valid position on the
	 * right then cursor will remain on the old position.
	 */
	private void moveCursorToRight() {
		if ((!cursorShow) || (cursorIndex == -1))
			return;
		for (int index = cursorIndex + 1;
			 index < graph.getGraph().size();
			 index++) {
			if (isValidCursorIndex(index)) {
				cursorIndex = index;
				cursorPoint = graph.getGraph().get(cursorIndex);
				break;
			}
		}
		updateCursorPoint();
	}
	/**
	 * Set values of cursor point to their respective label for user to see.
	 */
	private void setCursorValues() {
		if (cursorPoint == null) {
			lblValueForX.setValue("x: ");
			lblValueForY.setValue("y: ");
		} else {
			lblValueForX.setValue("x: " + Double.toString(cursorPoint.x));
			lblValueForY.setValue("y: " + Double.toString(cursorPoint.y));
		}
	}
	/**
	 * Update the position of the cursor. Do this for both UI frame and the
	 * GraphPanel widget. Also update labels showing x, y values of the cursor
	 * point.
	 */
	private void updateCursorPoint() {
		setCursorPoint();
		setGraphPanelCursorPoint();
		setCursorValues();
	}

	@WebServlet(urlPatterns = "/*", name = "GraphPlotterUIServlet", asyncSupported = true)
	@VaadinServletConfiguration(ui = GraphPlotterUI.class, productionMode = false)
	public static class GraphPlotterUIServlet extends VaadinServlet {
	}
}
