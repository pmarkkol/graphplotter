package fi.markkola.graphplotter.client.graphpanel;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Widget;

import com.vaadin.client.ui.AbstractComponentConnector;
import com.vaadin.shared.ui.Connect;

import fi.markkola.graphplotter.graphpanel.GraphPanel;
import fi.markkola.graphplotter.client.graphpanel.GraphPanelWidget;
import fi.markkola.graphplotter.client.graphpanel.GraphPanelState;

import com.vaadin.client.annotations.OnStateChange;
import com.vaadin.client.communication.StateChangeEvent;

@SuppressWarnings("serial")
@Connect(GraphPanel.class)
public class GraphPanelConnector extends AbstractComponentConnector {

	public GraphPanelConnector() {

	}

	@Override
	protected Widget createWidget() {
		return GWT.create(GraphPanelWidget.class);
	}

	@Override
	public GraphPanelWidget getWidget() {
		return (GraphPanelWidget) super.getWidget();
	}

	@Override
	public GraphPanelState getState() {
		return (GraphPanelState) super.getState();
	}

	@Override
	public void onStateChanged(StateChangeEvent stateChangeEvent) {
		super.onStateChanged(stateChangeEvent);
	}

	@OnStateChange("panelHeight")
	void updatePanelHeight() {
		getWidget().setPanelHeight(getState().panelHeight);
	}
	
	@OnStateChange("panelWidth")
	void updatePanelWidth() {
		getWidget().setPanelWidth(getState().panelHeight);
	}
	
	@OnStateChange("expression")
	void updateExpression() {
		getWidget().setExpression(getState().expression);
	}
	
	@OnStateChange("limits")
	void updateLimits() {
		getWidget().setLimits(getState().limits);
	}
	
	@OnStateChange("origo")
	void updateOrigo() {
		getWidget().setOrigo(getState().origo);
	}
	
	@OnStateChange("graph")
	void updateGraph() {
		getWidget().setGraph(getState().graph);
	}
	
	@OnStateChange("cursor")
	void updateCursor() {
		getWidget().setCursor(getState().cursor);
	}
}

