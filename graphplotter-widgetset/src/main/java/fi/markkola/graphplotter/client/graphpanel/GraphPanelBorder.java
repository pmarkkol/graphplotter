/**
 * 
 */
package fi.markkola.graphplotter.client.graphpanel;

import org.vaadin.gwtgraphics.client.shape.Rectangle;

/**
 * Class for creating a border for a GraphPanelWidget. 
 * 
 * @author Paavo Markkola
 */
public class GraphPanelBorder extends Rectangle {
	/**
	 * Construct a new GraphPanelBorder. Increase default stroke width and 
	 * fill insides with white.  
	 * 
	 * @param x			Start coordinate on x axis. 
	 * @param y			Start coordinate on y axis.
	 * @param width		Width of the border rectangle.
	 * @param height	Height of the border rectangle.
	 */
	public GraphPanelBorder(int x, int y, int width, int height) {
		super(x, y, width, height);
		setStrokeWidth(2);
		setStrokeColor("black");
		setFillColor("white");
	}

}
