/**
 * 
 */
package fi.markkola.graphplotter.client.graphpanel;

import org.vaadin.gwtgraphics.client.shape.Text;

/**
 * Class for displaying strings.   
 * 
 * @author Paavo Markkola
 */
public class GraphPanelText extends Text {
	/**
	 * Construct a new GraphPanelText instance. Use courier as default font and
	 * set size to 13. By default to display any string. the string can be 
	 * later set with setExpression().
	 *  
	 * @param x		The x-coordinate position in pixels.
	 * @param y		The y-coordinate position in pixels.
	 */
	public GraphPanelText(int x, int y) {
		super(x, y, "");
		setStrokeWidth(1);
		setFontFamily("Courier");
		setFontSize(13);
		setStrokeColor("black");
		setFillColor("black");
	}
	/**
	 * Set desired color.
	 * 
	 * @param color		Color to be used for the text.
	 */
	public void setColor(String color) {
		setStrokeColor(color);
		setFillColor(color);
	}
	/**
	 * Convert a limit value to a string and set it to be displayed. 
	 *  
	 * @param limit		The limit value to be displayed.
	 */
	public void setLimit(double limit) {
		setText(Double.toString(limit));
	}
	/**
	 * Set the mathematical expression to be displayed. 
	 * 
	 * @param expression	The mathematical expression to be displayed. 
	 */
	public void setExpression(String expression) {
		setText(expression);
	}
	
	
}
