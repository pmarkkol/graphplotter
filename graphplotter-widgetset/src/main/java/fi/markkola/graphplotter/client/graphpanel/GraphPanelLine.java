/**
 * 
 */
package fi.markkola.graphplotter.client.graphpanel;

import org.vaadin.gwtgraphics.client.Line;

/**
 * Class for creating a lines that connect adjacent points of a graph.  
 * 
 * @author Paavo Markkola
 */
public class GraphPanelLine extends Line {
	/**
	 * Construct a new GraphPanelLine instance. Default color is set to blue 
	 * and stroke increased to make the graph more distinct.
	 * 
	 * @param x1	The x-coordinate of the starting point in pixels.
	 * @param y1	The y-coordinate of the starting point in pixels.
	 * @param x2	The x-coordinate of the end point in pixels.
	 * @param y2	The y-coordinate of the end point in pixels.
	 */
	public GraphPanelLine(int x1, int y1, int x2, int y2) {
		super(x1, y1, x2, y2);
		setStrokeColor("blue");
		setStrokeWidth(2);
	}

}
