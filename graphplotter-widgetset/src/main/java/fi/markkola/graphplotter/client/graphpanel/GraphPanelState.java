package fi.markkola.graphplotter.client.graphpanel;

import java.util.List;

@SuppressWarnings("serial")
public class GraphPanelState extends com.vaadin.shared.AbstractComponentState {

	public int panelWidth;
	public int panelHeight;
	
	public String expression;
	
	public GraphPanelLimits limits;
	public GraphPanelPoint origo;
	public GraphPanelPoint cursor;
	
	public List<GraphPanelPoint> graph;
}