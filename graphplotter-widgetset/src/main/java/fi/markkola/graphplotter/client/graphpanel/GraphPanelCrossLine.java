/**
 * 
 */
package fi.markkola.graphplotter.client.graphpanel;

import org.vaadin.gwtgraphics.client.Line;

/**
 * Class for creating a cross lines for a GraphPanelWidget. 
 * 
 * @author Paavo Markkola
 */
public class GraphPanelCrossLine extends Line {
	/**
	 * Construct a new GraphPanelCrossLine with color set to the grey.
	 * 
	 * @param x1	The x-coordinate of the starting point in pixels.
	 * @param y1	The y-coordinate of the starting point in pixels.
	 * @param x2	The x-coordinate of the end point in pixels.
	 * @param y2	The y-coordinate of the end point in pixels.
	 */
	public GraphPanelCrossLine(int x1, int y1, int x2, int y2) {
		super(x1, y1, x2, y2);
		setStrokeColor("grey");
		setStrokeOpacity(1);
	}
}
