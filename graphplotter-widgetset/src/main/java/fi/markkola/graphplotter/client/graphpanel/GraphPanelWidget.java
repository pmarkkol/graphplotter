package fi.markkola.graphplotter.client.graphpanel;

import fi.markkola.graphplotter.client.graphpanel.GraphPanelBorder;
import fi.markkola.graphplotter.client.graphpanel.GraphPanelText;
import fi.markkola.graphplotter.client.graphpanel.GraphPanelPoint;
import fi.markkola.graphplotter.client.graphpanel.GraphPanelLimits;
import fi.markkola.graphplotter.client.graphpanel.GraphPanelCrossLine;
import java.util.ArrayList;
import java.util.List;
import org.vaadin.gwtgraphics.client.DrawingArea;
import org.vaadin.gwtgraphics.client.Group;
import org.vaadin.gwtgraphics.client.Line;

/**
 * 
 * 
 * @author Paavo Markkola
 */
public class GraphPanelWidget extends DrawingArea {
	public static final String CLASSNAME = "graphpanel";
	/**
	 * Default spacing in pixels for text outputs.
	 */
	private static final int DEFAULT_TEXT_SPACING = 5;
	/**
	 * Default panel size is 500*500 pixels.
	 */
	private static final int DEFAULT_PANEL_WIDTH = 500;
	private static final int DEFAULT_PANEL_HEIGHT = 500;
	/**
	 * Default spacing between each cross line in pixels.
	 */
	private static final int DEFAULT_CROSS_LINE_SPACING = 50;
	/**
	 * Default number of cross lines. Should be even so that both sides of
	 * the origo line have the same number of cross lines. 
	 */
	private static final int DEFAULT_CROSS_LINES_COUNT = 8;
	/**
	 * Panel size fields. Starts with the default ones but user may change 
	 * panel size at any time.
	 */
	private int panelWidth = DEFAULT_PANEL_WIDTH;
	private int panelHeight = DEFAULT_PANEL_HEIGHT;
	/**
	 * List of points that make up a graph.
	 */
	private List<GraphPanelPoint> graph;
	/**
	 * Minimum and maximum limits for both x and y axes. 
	 */
	private GraphPanelLimits limits;
	/**
	 * Point where both x and y are zero.
	 */
	private GraphPanelPoint origo;
	/**
	 * Point where cursor lines cross.
	 */
	private GraphPanelPoint cursor;
	/**
	 * the graph panel has a rectangle shaped border.
	 */
	private GraphPanelBorder border;
	/**
	 * Vertical and horizontal lines that cross at the origo. 
	 */
	private Line horizontalOrigoLine;
	private Line verticalOrigoLine;
	/**
	 * Group that holds cross lines.
	 */
	private Group crossLines;
	/**
	 * Group that holds lines connecting each point a graph.
	 */
	private Group graphLines;
	/**
	 * Group that holds lines that cross at cursor point.
	 */
	private Group cursorLines;
	/**
	 * GraphPanelText instance used to output expression string to graph panel. 
	 */
	private GraphPanelText expressionText;
	/**
	 * GraphPanelText instances used to output limits of both x and y axes.
	 */
	private GraphPanelText textLimitMinX;
	private GraphPanelText textLimitMaxX;
	private GraphPanelText textLimitMinY;
	private GraphPanelText textLimitMaxY;
	/**
	 * String containing mathematical expression.
	 */
	private String expression = "";
	/**
	 * Construct a new GraphPanel.
	 */
	public GraphPanelWidget() {
		super(DEFAULT_PANEL_WIDTH, DEFAULT_PANEL_HEIGHT);
		initGraphPanelBorder();
		initOrigo();
		initOrigoLines();
		initLimitsTexts();
		initCrossLines();
		initExpressionText();
		initGraphLines();
		initCursorLines();
	}
	/**
	 * Update all elements of the graph panel.
	 */
	private void updateGraphPanel() {
		setGraphPanelSize();
		setPanelBorderSize();
		setOrigoLines();
		setLimitTextPositions();
		setCrossLines();
		setGraphLines();
	}
	/**
	 * Set size for the graph panel. 
	 */
	private void setGraphPanelSize() {
		setWidth(panelWidth);
		setHeight(panelHeight);
	}
	/**
	 * Initialize origo point. 
	 */
	private void initOrigo() {
		origo = new GraphPanelPoint(DEFAULT_PANEL_WIDTH / 2, 
									DEFAULT_PANEL_HEIGHT / 2);
	}
	/**
	 * Initialize border for the graph panel.  
	 */
	private void initGraphPanelBorder() {
		border = new GraphPanelBorder(0, 0, panelWidth, panelHeight);
		add(border);
	}
	/**
	 * Set border size for the graph panel.
	 */
	private void setPanelBorderSize() {
		border.setWidth(panelWidth);
		border.setHeight(panelHeight);
	}
	/**
	 * Initialize origo lines that are set cross where x and y are both 0. 
	 */
	private void initOrigoLines() {
		horizontalOrigoLine = new Line(0, 0, 0, 0);
		verticalOrigoLine = new Line(0, 0, 0, 0);
		horizontalOrigoLine.setStrokeColor("black");
		verticalOrigoLine.setStrokeColor("black");
		horizontalOrigoLine.setStrokeWidth(2);
		verticalOrigoLine.setStrokeWidth(2);
		setOrigoLines();
	}
	/**
	 * Update both vertical and horizontal origo lines.
	 */
	private void setOrigoLines() {
		setHorizontalOrigoLine();
		setVerticalOrigoLine();
	}
	/**
	 * Check if vertical origo line is set to visible. If not remove it from 
	 * the graph panel. If yes, then update the location of the line.  
	 */
	private void setVerticalOrigoLine() {
		remove(verticalOrigoLine);
		if (origo.getX() >= 0)
			add(verticalOrigoLine);
		setVerticalOrigoLinePosition();
	}
	/**
	 * Set vertical origo line according to the origo point.
	 */
	private void setVerticalOrigoLinePosition() {
		verticalOrigoLine.setX1(origo.getX());
		verticalOrigoLine.setY1(0);
		verticalOrigoLine.setX2(origo.getX());
		verticalOrigoLine.setY2(getHeight());
	}
	/**
	 * Check if horizontal origo line is set to visible. If not remove it from 
	 * the graph panel. If yes, then update the location of the line.  
	 */
	private void setHorizontalOrigoLine() {
		remove(horizontalOrigoLine);
		if (origo.getY() >= 0)
			add(horizontalOrigoLine);
		setHorizontalOrigoLinePosition();
	}
	/**
	 * Set horizontal origo line according to the origo point.
	 */
	private void setHorizontalOrigoLinePosition() {
		horizontalOrigoLine.setX1(0);
		horizontalOrigoLine.setY1(origo.getY());
		horizontalOrigoLine.setX2(getWidth());
		horizontalOrigoLine.setY2(origo.getY());
	}
	/**
	 * Initialize text elements that show minimum and maximum limits for both
	 * axes. 
	 */
	private void initLimitsTexts() {
		limits = new GraphPanelLimits();
		textLimitMinX = new GraphPanelText(0, 0);
		textLimitMaxX = new GraphPanelText(0, 0);
		textLimitMinY = new GraphPanelText(0, 0);
		textLimitMaxY = new GraphPanelText(0, 0);
		setLimitValues();
		setLimitTextPositions();
		add(textLimitMinX);
		add(textLimitMaxX);
		add(textLimitMinY);
		add(textLimitMaxY);
	}
	/**
	 * Position limit text elements at correct places.
	 */
	private void setLimitTextPositions() {
		textLimitMinX.setX(DEFAULT_TEXT_SPACING);
		textLimitMinX.setY(getHeight() / 2 - DEFAULT_TEXT_SPACING);
		textLimitMaxX.setX(getWidth() - textLimitMaxX.getTextWidth() - 
														DEFAULT_TEXT_SPACING);
		textLimitMaxX.setY(getHeight() / 2 - DEFAULT_TEXT_SPACING);
		textLimitMinY.setX(getWidth() / 2 + DEFAULT_TEXT_SPACING);
		textLimitMinY.setY(getHeight() - DEFAULT_TEXT_SPACING);
		textLimitMaxY.setX(getWidth() / 2 + DEFAULT_TEXT_SPACING);
		textLimitMaxY.setY(textLimitMaxY.getTextHeight());
	}
	/**
	 * Set limit values to the to the text elements.
	 */
	private void setLimitValues() {
		textLimitMinX.setLimit(limits.getLimitMinX());
		textLimitMaxX.setLimit(limits.getLimitMaxX());
		textLimitMinY.setLimit(limits.getLimitMinY());
		textLimitMaxY.setLimit(limits.getLimitMaxY());
	}
	/**
	 * Initialize cross line set.
	 */
	private void initCrossLines() {
		crossLines = new Group();
		setCrossLines();
	}
	/**
	 * Remove cross lines from the graph panel and add them again. Use group 
	 * crossLines for this purpose. 
	 */
	private void setCrossLines() {
		remove(crossLines);
		crossLines.clear();
		setVerticalCrossLines(getVerticalCrossLineSpacing());
		setHorizontalCrossLines(getHorizontalCrossLineSpacing());
		add(crossLines);
	}
	/**
	 * Get spacing between vertical cross lines.
	 * 
	 * @return	The number of pixels between vertical cross lines.
	 */
	private int getVerticalCrossLineSpacing() {
		return getWidth() / getNumberOfCrossLines();
	}
	/**
	 * Get spacing between horizontal cross lines.
	 * 
	 * @return	The number of pixels between horizontal cross lines.
	 */
	private int getHorizontalCrossLineSpacing() {
		return getHeight() / getNumberOfCrossLines();
	}
	/**
	 * Get number of cross lines. Use constant DEFAULT_NUMBER_OF_CROSS_LINES as
	 * initial number of lines. Add or subtract lines if spacing is less then 
	 * or more than DEFAULT_CROSS_LINE_SPACING. 
	 *  
	 * @return	The number of cross lines.   
	 */
	private int getNumberOfCrossLines() {
		int numberOfLines = DEFAULT_CROSS_LINES_COUNT;
		if ((getWidth() / numberOfLines) < DEFAULT_CROSS_LINE_SPACING)
			while ((getWidth() / numberOfLines) < DEFAULT_CROSS_LINE_SPACING)
				if (numberOfLines > 2)
					numberOfLines -= 2;
				else
					break;
		else
			while ((getWidth() / numberOfLines) > DEFAULT_CROSS_LINE_SPACING)
				numberOfLines += 2;
		return numberOfLines;
	}
	/**
	 * Set vertical cross lines. Start from vertical origo line if it exist 
	 * and proceed in both directions separately if necessary.
	 *
	 * @param xSpacing	Spacing in between each cross line in pixels.
	 */
	
	private void setVerticalCrossLines(int xSpacing) {
		for (int x = getVerticalOrigoLinePosition(); x < getWidth(); x += xSpacing)
			crossLines.add(new GraphPanelCrossLine(x, 0, x, getHeight()));
		for (int x = getVerticalOrigoLinePosition(); x > 0; x -= xSpacing)
			crossLines.add(new GraphPanelCrossLine(x, 0, x, getHeight()));
	}
	/**
	 * Set horizontal cross lines. Start from horizontal origo line if it 
	 * exist and proceed in both directions separately if necessary.
	 *    
	 * @param ySpacing	Spacing in between each cross line in pixels.
	 */
	private void setHorizontalCrossLines(int ySpacing) {
		for (int y = getHorizontalOrigoLinePosition(); y < getHeight(); y += ySpacing)
			crossLines.add(new GraphPanelCrossLine(0, y, getWidth(), y));
		for (int y = getHorizontalOrigoLinePosition(); y > 0; y -= ySpacing)
			crossLines.add(new GraphPanelCrossLine(0, y, getWidth(), y));
	}

	/**
	 * Get position of the vertical origo line. If the line does not exist 
	 * then return vertical center of the canvas.
	 * 
	 * @return	Position of the vertical origo line.
	 */
	private int getVerticalOrigoLinePosition() {
		if (origo.getX() >= 0)
			return origo.getX();
		else
			return getWidth() / 2;
	}
	/**
	 * Get position of the horizontal origo line. If the line does not exist 
	 * then return horizontal center of the canvas.
	 * 
	 * @return	Position of the horizontal origo line.
	 */
	private int getHorizontalOrigoLinePosition() {
		if (origo.getY() >= 0)
			return origo.getY();
		else
			return getHeight() / 2;
	}
	/**
	 * Initialize and add expression as string to the top left corner of the 
	 * graph panel. Use the same color as for the graph.
	 */
	private void initExpressionText() {
		expressionText = new GraphPanelText(0, 0);
		expressionText.setX(5);
		expressionText.setY(15);
		expressionText.setColor("blue");
		setExpressionText();
		add(expressionText);
	}
	/**
	 * Set expression string to graph panel. 
	 */
	private void setExpressionText() {
		expressionText.setText(expression);
	}
	/**
	 * initialize group to hold lines that make up a graph.
	 */
	private void initGraphLines() {
		graph = new ArrayList<>();
		graphLines = new Group();
	}
	/**
	 * Update the graphLines group to draw the graph to the panel. First remove
	 * previous graph from the panel and clear the group. Loop through each 
	 * point on the graph and create lines between two points in case both are 
	 * visible on the panel. Finally draw the graph by adding the group to the
	 * panel.
	 */
	private void setGraphLines() {
		remove(graphLines);
		graphLines.clear();
		if (graph != null)
			for (int index = 0; index < graph.size(); index++) {
				if (index + 1 >= graph.size())
					break;
				if ((isVisible(graph.get(index))) && 
					(isVisible(graph.get(index + 1))))
					graphLines.add(createGraphLine(graph.get(index), 
												   graph.get(index + 1)));	
			}
		add(graphLines);
	}
	/**
	 * Check if GraphPanelPoint is visible on panel. To be visible location of
	 * the point must be within size limits of the panel.
	 * 
	 * @param point		GraphPoint to be checked for visibility.
	 * @return			True if visible and false if not.
	 */
	private boolean isVisible(GraphPanelPoint point) {
		if (point == null)
			return false;
		if ((point.getX() >= 0) && (point.getX() <= getWidth()) && 
			(point.getY() >= 0) && (point.getY() <= getHeight()))
			return true;
		return false;
	}
	/**
	 * Create a line between two points on a graph. 
	 * 
	 * @param start	Start point of the line.
	 * @param end	End point of the line.
	 */
	private Line createGraphLine(GraphPanelPoint start, GraphPanelPoint end) {
		return new GraphPanelLine(start.getX(), start.getY(), 
								  end.getX(), end.getY());
	}
	/**
	 * Initialize group containing cursor lines.
	 */
	private void initCursorLines() {
		cursorLines = new Group();
		setCursorLines();
	}
	/**
	 * Set cursor lines according to cursor point. If cursor point is null then
	 * remove cursor lines from display. 
	 */
	private void setCursorLines() {
		remove(cursorLines);
		cursorLines.clear();
		if (cursor != null)
			createCursorLines();
		add(cursorLines);
	}
	/**
	 * Create new cursor lines.
	 */
	private void createCursorLines() {
		Line verticalCursorLine = new Line(cursor.getX(), 0, cursor.getX(), getHeight());
		Line horizontalCursorLine = new Line(0, cursor.getY(), getWidth(), cursor.getY());
		verticalCursorLine.setStrokeColor("red");
		verticalCursorLine.setStrokeWidth(1);
		horizontalCursorLine.setStrokeColor("red");
		horizontalCursorLine.setStrokeWidth(1);
		cursorLines.add(verticalCursorLine);
		cursorLines.add(horizontalCursorLine);
	}
	/**
	 * Set the width of the graph panel. 
	 * 
	 * @param width Width of the graph panel.
	 */
	public void setPanelWidth(int width) {
		if (width < 0)
			return;
		panelWidth = width;
		updateGraphPanel();
	}
	/**
	 * Set the height of the graph panel. 
	 * 
	 * @param height Height of the graph panel.
	 */
	public void setPanelHeight(int height) {
		if (height < 0)
			return;
		panelHeight = height;
		updateGraphPanel();
	}
	/**
	 * Set the mathematical expression.
	 * 
	 * @param expression Mathematical expression as a string.
	 */
	public void setExpression(String expression) {
		if (expression != null)
			this.expression = expression;
		setExpressionText();
	}
	/**
	 * Set real value limits of both x and y axes according to the given 
	 * parameters.
	 * 
	 * @param limits	Real value limits for both x and y axes.	
	 */
	public void setLimits(GraphPanelLimits limits) {
		if (limits != null)
			this.limits = limits;
		setLimitValues();
	}
	/**
	 * Set origo to the given point.
	 * 
	 * @param origo	Point where x and y are both zero.
	 */
	public void setOrigo(GraphPanelPoint origo) {
		if (origo != null)
			this.origo = origo;
		setOrigoLines();
		setCrossLines();
	}
	/**
	 * Set the given list of GraphPanelPoints to be drawn as graph on the 
	 * graph panel. 
	 * 
	 * @param graph	List containing all points of a graph to be drawn.
	 */
	public void setGraph(List<GraphPanelPoint> graph) {
		this.graph = graph;
		setGraphLines();
	}
	/**
	 * 
	 * @param cursor	Point where cursor lines cross.
	 */
	public void setCursor(GraphPanelPoint cursor) {
		this.cursor = cursor;
		setCursorLines();
	}
}
