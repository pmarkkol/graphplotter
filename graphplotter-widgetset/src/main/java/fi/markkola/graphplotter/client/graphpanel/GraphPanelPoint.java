/**
 * 
 */
package fi.markkola.graphplotter.client.graphpanel;

import java.io.Serializable;

/**
 * This class represents a discrete point of a graph on a Cartesian coordinate 
 * system, with x and y axes, that is mapped to a graphical presentation on a 
 * canvas.
 *   
 * @author Paavo Markkola
 */
public class GraphPanelPoint implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2256531416480971681L;
	/**
	 * Horizontal position of the point on the canvas 
	 */
	private int x;
	/**
	 * Vertical position of the point on the canvas
	 */
	private int y;
	/**
	 * Default constructor for GraphPanelPoint.
	 */
	public GraphPanelPoint() {
		x = 0;
		y = 0;
	}
	/**
	 * Construct a new GraphPanelPoint.
	 * 
	 * @param x	Horizontal position of the point on the canvas 
	 * @param y	Vertical position of the point on the canvas
	 */
	public GraphPanelPoint(int x, int y) {
		this.x = x;
		this.y = y;
	}
	/**
	 * Get horizontal position of the point on the canvas.
	 *  
	 * @return	Horizontal position of the point on the canvas. 
	 */
	public int getX() {
		return x;
	}
	/**
	 * Set horizontal position of the point on the canvas.
	 *  
	 * @param x	Horizontal position of the point on the canvas.
	 */
	public void setX(int x) {
		this.x = x;
	}
	/**
	 * Get vertical position of the point on the canvas.
	 *  
	 * @return	Vertical position of the point on the canvas. 
	 */
	public int getY() {
		return y;
	}
	/**
	 * Set vertical position of the point on the canvas.
	 *  
	 * @param y	Vertical position of the point on the canvas.
	 */
	public void setY(int y) {
		this.y = y;
	}
}
