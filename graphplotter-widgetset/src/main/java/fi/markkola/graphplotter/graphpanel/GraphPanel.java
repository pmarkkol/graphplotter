package fi.markkola.graphplotter.graphpanel;

import java.util.ArrayList;
import java.util.List;
import com.vaadin.ui.AbstractComponent;
import fi.markkola.graphplotter.client.graphpanel.GraphPanelLimits;
import fi.markkola.graphplotter.client.graphpanel.GraphPanelPoint;
import fi.markkola.graphplotter.client.graphpanel.GraphPanelState;

/**
 * 
 * @author Paavo Markkola
 */
@SuppressWarnings("serial")
public class GraphPanel extends AbstractComponent {
	/**
	 * A list containing all points that make up a graph. 
	 */
	private List<GraphPanelPoint> graph;
	/**
	 * Construct a new GraphPanel instance.
	 */
	public GraphPanel() {
		graph = new ArrayList<>();
	}
	/**
	 * Get state container for the GraphPanel instance.
	 */
	@Override
	public GraphPanelState getState() {
		return (GraphPanelState) super.getState();
	}
	/**
	 * Add new graph point to the list of points representing a graph.
	 *  
	 * @param x		X coordinate of the point to be added.
	 * @param y		Y coordinate of the point to be added.
	 */
	public void addGraphPoint(int x, int y) {
		graph.add(new GraphPanelPoint(x, y));
	}
	/**
	 * Reset the list containing all graph points. This allows new graph to be 
	 * defined for the GraphPanel.
	 */
	public void clearGraph() {
		graph.clear();
	}
	/**
	 * Set the graph to the GraphPanel instance.
	 */
	public void updateGraph() {
		getState().graph = this.graph; 
	}
	/**
	 * Set the side, i.e., width and height, of the GraphPanel.
	 * 
	 * @param width		Width of the GraphPanel in pixels.
	 * @param height	Height of the GraphPanel in pixels.
	 */
	public void setSize(int width, int height) {
		getState().panelWidth = width;
		getState().panelHeight = height;
	}
	/**
	 * Set the mathematical expression in string format for the GraphPanel. 
	 * This string represents the graph displayed on the GraphPanel. 
	 * 
	 * @param expression	Mathematical expression as a String. 
	 */
	public void setExpression(String expression) {
		getState().expression = expression;
	}
	/**
	 * Set maximum and minimum limits for x and y axes for GraphPanel. These 
	 * values are shown at the end of both axes.  
	 * 
	 * @param minX	Minimum limit for x axis.
	 * @param maxX	Maximum limit for x axis.
	 * @param minY	Minimum limit for y axis.
	 * @param maxY	Maximum limit for y axis.
	 */
	public void setLimits(double minX, double maxX, double minY, double maxY) {
		GraphPanelLimits limits = new GraphPanelLimits();
		limits.setLimitsForX(minX, maxX);
		limits.setLimitsForY(minY, maxX);
		getState().limits = limits;
	}
	/**
	 * Set location of origo within the drawing area of the GraphPanel instance.
	 * 
	 * @param x		X coordinate of the origo point.
	 * @param y		Y coordinate of the origo point.
	 */
	public void setOrigo(int x, int y) {
		getState().origo = new GraphPanelPoint(x, y);
	}
	/**
	 * Set location of cursor point within the drawing area of the GraphPanel
	 * instance.
	 * 
	 * @param x		X coordinate of the origo point.
	 * @param y		Y coordinate of the origo point.
	 */
	public void setCursor(int x, int y) {
		getState().cursor = new GraphPanelPoint(x, y);
	}
	/**
	 * Remove cursor from the GraphPanel instance.
	 */
	public void clearCursor() {
		getState().cursor = null;
	}
}
